#include "nodeliststructure.h"
#include <stdio.h>

/********************************************************************
* createNode - A function that sets up a Node structure             *
*                                                                   *
* Inputs: The ID, latitude, and longitude of the Node               *
*                                                                   *
* Outputs: Returns a pointer to the newly created Node              *
********************************************************************/
Node* createNode(long, double, double);

/********************************************************************
* printNode - A function that prints out the details of a Node to a *
* file                                                              *
*                                                                   *
* Inputs: A pointer to the file to print to, and a poitner to the   *
* Node to print                                                     *
*                                                                   *
* Outputs: No values returned. Prints out the lat and lon of the    *
* specified Node to the specified file                              *
********************************************************************/
void printNode(FILE*, Node*);

/********************************************************************
* deleteNode - A function that frees up the memory being used by    *
* a Node                                                            *
*                                                                   *
* Inputs: A pointer to the Node to delete                           *
*                                                                   *
* Outputs: No values returned. Frees the memory being used by the   *
* specified Node                                                    *
********************************************************************/
void deleteNode(Node*);

/********************************************************************
* createNodeList - A function that sets up a NodeList structure     *
*                                                                   *
* Inputs: None                                                      *
*                                                                   *
* Outputs: A pointer to the newly created NodeList                  *
********************************************************************/
NodeList* createNodeList();

/********************************************************************
* addToNodeList - A function that adds a Node to a NodeList         *
*                                                                   *
* Inputs: A pointer to the Node to add, and a pointer to the        *
* NodeList to be added to                                           *
*                                                                   *
* Outputs: No values returned. Adds the specified Node to the start *
* of the specified NodeList                                         *
********************************************************************/
void addToNodeList(NodeList*, Node*);

/********************************************************************
* printNodeList - A function that prints out the details of each    *
* pair of connected Nodes in a NodeList to a file                   *
*                                                                   *
* Inputs: A pointer to the file to write to, and a pointer to the   *
* NodeList to print                                                 *
*                                                                   *
* Outputs: No values returned. Prints out the lat and lon of each   *
* pair of connected Nodes in the NodeList given                     *
********************************************************************/
void printNodeList(FILE*, NodeList*);

/********************************************************************
* findNodeInList - A function that finds a Node in a NodeList with  *
* a given ID                                                        *
*                                                                   *
* Inputs: A pointer to the NodeList to search, and the ID to search *
* for                                                               *
*                                                                   *
* Outputs: A pointer to the Node with the specified ID, or NULL if  *
* no Node is found                                                  *
********************************************************************/
Node* findNodeInList(NodeList*, long);

/********************************************************************
* deleteNodeList - A function that frees up the memory being used   *
* by a NodeList                                                     *
*                                                                   *
* Inputs: A pointer to the NodeList to delete                       *
*                                                                   *
* Outputs: no values returned. Frees up the memory being used by    *
* the specified NodeList                                            *
********************************************************************/
void deleteNodeList(NodeList*);
