Setup (Ubuntu)
---

1. Ensure gcc and make are installed
```sh
sudo apt install gcc
sudo apt install make
```

2. For visualising the output, ensure gnuplot is installed
```sh
sudo apt-get install gnuplot-qt
```
3. Navigate to the cloned repository

4. Build the source code
```sh
make
```

5. (Optional) make can also be used to clean the repository
```sh
make clean
```

Running
---

To start the application, run the compiled executable
```sh
./graphtraverse <map_file_path>
```
The default map "Final_Map.map" is already provided, containing map data of a small region of Leeds

The application allows a number of commands:

- **help**: displays each command and its use
- **path <node_1_id> <node_2_id>**: Computes and saves the shortest path between node_1 and node_2 in the map **<sup>*</sup>** 
- **distance=on/off**: Toggle display of total distance when computing path
- **links=on/off**: Toggle display of # of links used when computing path
- **print**: Writes map out to an output plot output/graph.out
- **path=on/off**: Toggle display of path in output plot
- **exit**: exits the application

**<sup>*</sup>** Currently, the only way to select the start and end node is by copying the ids from the file "node_ids.txt". This is rather cumbersome, and is something I would like to change at some point. 

Output
---

Outputs are contained within the output directory. graph.out contains the plot data, and print.gnu is a script that can be used to visualise the map in gnuplot

```sh
cd output
gnuplot --persist print.gnu
```

![example output displayed in gnuplot](https://i.imgur.com/QMI70Ux.png)

Attribution
---

Repository icon made by <a href="https://www.flaticon.com/authors/freepik" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon"> www.flaticon.com</a>
