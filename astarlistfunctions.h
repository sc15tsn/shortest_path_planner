#include "astarliststructure.h"


/********************************************************************
* createAStarNode - A function that sets up an AStarNode structure  *
* to be used                                                        *
*                                                                   *
* Inputs: A pointer to the underlying node value, doubles for the   *
* gValue and fValue, and an AStarNode pointer to the most efficient *
* previous node                                                     *
*                                                                   *
* Outputs: Returns the newly initialised node                       *
********************************************************************/
AStarNode* createAStarNode(Node*, double, double, AStarNode*);

/********************************************************************
* deleteAStarNode - A function that frees the memory being used by  *
* an AStarNode without deleting the underlying Node structure       *
*                                                                   *
* Inputs: A pointer to the AStarNode to delete                      *
*                                                                   *
* Outputs: No values returned. Frees up the memory being used by    *
* the AStarNode                                                     *
********************************************************************/
void deleteAStarNode(AStarNode*);

/********************************************************************
* createAStarList - A function that sets up a new AStarList         *
* structure for use                                                 *
*                                                                   *
* Inputs: None                                                      *
*                                                                   *
* Outputs: A pointer to the created AStarList                       *
********************************************************************/
AStarList* createAStarList();

/********************************************************************
* addToAStarList - A function that adds an AStarNode to an          *
* AStarList                                                         *
*                                                                   *
* Inputs: Pointers to the AStarList to add to, and the AStarNode to *
* add                                                               *
*                                                                   *
* Outputs: No values returned. Adds the given AStarNode to the      *
* end of the given AStarList                                        *
********************************************************************/
void addToAStarList(AStarList*, AStarNode*);

/********************************************************************
* getMinDistNode - Gets the node in an AStarList with the lowest    *
* fValue                                                            *
*                                                                   *
* Inputs: A pointer to the AStarList to search                      *
*                                                                   *
* Outputs: Returns the AStarNode with the smallest fValue, or NULL  *
* if the list is empty                                              *
********************************************************************/
AStarNode* getMinDistNode(AStarList*);

/********************************************************************
* moveAStarNode - A function that moves an AStarNode from one list  *
* to another                                                        *
*                                                                   *
* Inputs: Pointers to the source list, the node to move, and the    *
* destination list                                                  *
*                                                                   *
* Outputs: No values returned. Moves the given AStarNode from one   *
* list to the end of the other list                                 *
********************************************************************/
void moveAStarNode(AStarList*, AStarNode*, AStarList*);

/********************************************************************
* findAStarNodeInList - A function that finds an AStarNode in an    *
* AStarList                                                         *
*                                                                   *
* Inputs: A pointer to the list to search, and the ID of the node   *
* to find                                                           *
*                                                                   *
* Outputs: Returns the AStarNode whose underlying Node has the      *
* given ID                                                          *
********************************************************************/
AStarNode* findAStarNodeInList(AStarList*, long);

/********************************************************************
* deleteAStarList - A function that frees up the memory being used  *
* by an AStarList                                                   *
*                                                                   *
* Inputs: A pointer to the list to delete                           *
*                                                                   *
* Outputs: No values returned. Frees up the memory being used by    *
* the AStarList                                                     *
********************************************************************/
void deleteAStarList(AStarList*);
