#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "fileinout.h"
#include "nodelistfunctions.h"


FILE* openFile(char* fileName)
{

    //Attempt to open the file in read mode
    FILE* readFile = fopen(fileName, "r");

    //If the file can't be opened, print an error and return NULL
    if(readFile == NULL)
    {

        printf("Error: Input file \"%s\" could not be accessed\n", fileName);
        return NULL;

    }

    //Otherwise, return the FILE pointer
    return readFile;

}


int validateFile(FILE* fileToValidate)
{

    int lineCount = 1;    //The current line in the file
    char prevTagLink = 0; //Boolean value. 1 if the previous tag was a link tag, otherwise 0
    char prevTagNode = 0; //Boolean value. 1 if the previous tag was a node tag, otherwise 0


    //Move past the bounding tag to the second line, increase the line count, and read the next 6 characters
    while(fgetc(fileToValidate) != '\n');
    lineCount++;
    char* tempStorage = malloc(6);
    fread(tempStorage, 6, 1, fileToValidate);

    //Start validating the link tags of the file...
    while(1)
    {

        //If it is not the start of a link tag...
        if(strcmp(tempStorage, "<link ") != 0)
        {

            //If the previous tag was a link tag, stop searching for link tags
            if(prevTagLink == 1)
                break;

            //Otherwise, print an error message
            printf("Error: Input file invalid link tag, or no link tags present (line %d)\n", lineCount);
            return -1;

        }

        //Jump to the end of the line and read the last 7 characters
        while(fgetc(fileToValidate) != '\n');
        fseek(fileToValidate, -7, SEEK_CUR);
        free(tempStorage);
        tempStorage = malloc(6);
        fread(tempStorage, 6, 1, fileToValidate);

        //If it is not the end of a link tag...
        if(strcmp(tempStorage, "/link>") != 0)
        {

            //Print an error message
            printf("Error: Input file link tag not closed (line %d)\n", lineCount);
            return -1;

        }

        //Move to the next line, increase the line count, and read the next 6 characters
        fseek(fileToValidate, 1, SEEK_CUR);
        lineCount++;
        free(tempStorage);
        tempStorage = malloc(6);
        fread(tempStorage, 6, 1, fileToValidate);

        //Since at least one link tag has been found, set the value to 1
        prevTagLink = 1;

    }

    //Start validating the node tags of the file...
    while(1)
    {

        //If it is not the start of a node tag...
        if(strcmp(tempStorage, "<node ") != 0)
        {

            if(prevTagNode == 1)
                break;

            //Print an error message
            printf("Error: Input file invalid node tag, or no node tags present (line %d)\n", lineCount);
            return -1;

        }

        //Jump to the end of the line and read the last 7 characters
        while(fgetc(fileToValidate) != '\n');
        fseek(fileToValidate, -7, SEEK_CUR);
        free(tempStorage);
        tempStorage = malloc(6);
        fread(tempStorage, 6, 1, fileToValidate);

        //If it is not the end of a node tag...
        if(strcmp(tempStorage, "/node>") != 0)
        {

            //Print an error message
            printf("Error: Input file node tag not closed (line %d)\n", lineCount);
            return -1;

        }

        //Move to the next line, increase the line count, and read the next 6 characters
        fseek(fileToValidate, 1, SEEK_CUR);
        lineCount++;
        free(tempStorage);
        tempStorage = malloc(6);
        fread(tempStorage, 6, 1, fileToValidate);

        //Since at least one node tag has been found, set the value to 1
        prevTagNode = 1;

    }

    printf("Input file tags present...\n");

    //Start scanning backwards for the end of the last node tag

    while(strcmp(tempStorage, "/node>") != 0)
    {

        fseek(fileToValidate, -7, SEEK_CUR);
        fread(tempStorage, 6, 1, fileToValidate);
    }

    free(tempStorage);

    //Once the end has been found, get and return the size
    int fileSize = ftell(fileToValidate);
    rewind(fileToValidate);
    return fileSize;

}


char* storeFile(FILE* fileToStore)
{

    //Validate the file and get the length
    int fileSize = validateFile(fileToStore);

    //If there was a file error, return NULL
    if(fileSize == -1)
        return NULL;

    //Move past the bounding tag
    while(fgetc(fileToStore) != '\n');

    //Allocate enough memory to store the file, and copy the file into the variable
    char* storedFile = malloc(fileSize - ftell(fileToStore));
    fread(storedFile, (fileSize - ftell(fileToStore)), 1, fileToStore);

    //Close the input file
    fclose(fileToStore);

    return storedFile;

}


void printGraph(NodeList* graphToPrint, NodeList* pathToPrint)
{

    //Set up the output files. Only set up one for the path if a path is present
    FILE* graphFile = fopen("output/graph.out", "w");
    FILE* pathFile = NULL;
    FILE* printFile = fopen("output/print.gnu", "w");

    //Print out the graph information to the graph output file
    printNodeList(graphFile, graphToPrint);

    //Print the graph printing settings to the gnuplot file
    fprintf(printFile, "plot \'graph.out\' with linespoints pointtype 7 ps 0.3");

    //If there is a path to print, set up the path file, print the path details to it, and add the path print settings to the gnuplot file
    if(pathToPrint != NULL)
    {

        pathFile = fopen("output/path.out", "w");
        printNodeList(pathFile, pathToPrint);
        fprintf(printFile, ", \'path.out\' with linespoints pointtype 7 ps 0.3");
        fclose(pathFile);

    }

    //Close the files

    fclose(graphFile);
    fclose(printFile);

}
