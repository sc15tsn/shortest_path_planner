#include "linkliststructure.h"

/********************************************************************
* createLink - A function that sets up a new Link structure         *
*                                                                   *
* Inputs: The ID and length of the Link, and the Node at the other  *
* end of the Link                                                   *
*                                                                   *
* Outputs: Returns a pointer to the newly created Link              *
********************************************************************/
Link* createLink(long, Node*, double);

/********************************************************************
* deleteLink - A function that frees up the memory being used by a  *
* Link                                                              *
*                                                                   *
* Inputs: A pointer to the Link to delete                           *
*                                                                   *
* Outputs: No values returned. Frees up the memory being used by    *
* the specified Link                                                *
********************************************************************/
void deleteLink(Link*);

/********************************************************************
* createLinkList - A function that sets up a LinkList structure     *
*                                                                   *
* Inputs: None                                                      *
*                                                                   *
* Outputs: Returns a pointer to the newly created LinkList          *
********************************************************************/
LinkList* createLinkList();

/********************************************************************
* addToLinkList - A function that adds a Link to a LinkList         *
*                                                                   *
* Inputs: A pointer to the LinkList to add to, and a pointer to the *
* Link to add to it                                                 *
*                                                                   *
* Outputs: No values returned. Adds the specified Link to the       *
* beginning of the specified LinkList                               *
********************************************************************/
void addToLinkList(LinkList*, Link*);

/********************************************************************
* deleteLinkList - A function that frees up the memory being used   *
* by a LinkList                                                     *
*                                                                   *
* Inputs: A pointer to the LinkList to delete                       *
*                                                                   *
* Outputs: No values returned. Frees the memory being used by the   *
* given LinkList                                                    *
********************************************************************/
void deleteLinkList(LinkList*);
