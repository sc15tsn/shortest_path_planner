#include "nodelistfunctions.h"
#include "linklistfunctions.h"
#include <stdio.h>
#include <stdlib.h>

Node* createNode(long newId, double newLatitude, double newLongitude)
{

    //Allocate enough memory for the node
    Node* newNode = (Node*) malloc(sizeof(Node));

    //Initialise the fields with the given values
    newNode -> nodeId = newId;
    newNode -> latitude = newLatitude;
    newNode -> longitude = newLongitude;
    newNode -> nodeLinks = createLinkList();
    newNode -> nextNode = NULL;

    return newNode;

}


void printNode(FILE* fileToPrintTo, Node* nodeToPrint)
{

    fprintf(fileToPrintTo, "%lf\t%lf\n", nodeToPrint -> longitude, nodeToPrint -> latitude);

}


void deleteNode(Node* nodeToDelete)
{

    //Start by deleting the list of links for that node
    deleteLinkList(nodeToDelete -> nodeLinks);

    //Then delete the node itself
    free(nodeToDelete);

}


NodeList* createNodeList()
{

    //Allocate enough memory for the NodeList
    NodeList* newNodeList = (NodeList*) malloc(sizeof(NodeList));

    //Initialise the NodeList fields
    newNodeList -> firstNode = NULL;

    return newNodeList;

}


void addToNodeList(NodeList* listToAddTo, Node* nodeToAdd)
{

    //Make the new node the start of the list, and make the new node point to the previous start of the list
    nodeToAdd -> nextNode = listToAddTo -> firstNode;
    listToAddTo -> firstNode = nodeToAdd;

}


void printNodeList(FILE* fileToWriteTo, NodeList* NodeListToPrint)
{

    //Get the first node in the list
    Node* currentNode = NodeListToPrint -> firstNode;

    //While the end of the list has not been reached...
    while(currentNode != NULL)
    {

        //Get the first link attached to this node
        Link* currentLink = currentNode -> nodeLinks -> firstLink;

        //While there are more links attached to this node...
        while(currentLink != NULL)
        {

            //Get the node on the other end of the link, print out the details of the adjacent nodes, and move on to the next link
            Node* adjacentNode = currentLink -> adjNode;

            printNode(fileToWriteTo, currentNode);
            printNode(fileToWriteTo, adjacentNode);
            fprintf(fileToWriteTo, "\n");

            currentLink = currentLink -> nextLink;

        }

      //Once all of the attached nodes have been printed, move on to the next node
      currentNode = currentNode -> nextNode;

    }

}


Node* findNodeInList(NodeList* listToSearch, long idToFind)
{

    //Get the first node
    Node* currentNode = listToSearch -> firstNode;

    //While the end of the list has not been reached...
    while(currentNode != NULL)
    {

        //If the ID of the current Node is the desired ID, return that node
        if(currentNode -> nodeId == idToFind)
            return currentNode;

        //Otherwise move on to the next node
        currentNode = currentNode -> nextNode;

    }

    //If the end of the list has been reached and no node has been found, return NULL
    return NULL;

}


void deleteNodeList(NodeList* listToDelete)
{

    //Get the first node
    Node* currentNode = listToDelete -> firstNode;

    //While the end of the list has not been reached
    while(currentNode != NULL)
    {

        //Get the next node
        Node* nextNode = currentNode -> nextNode;

        //Delete the current node
        deleteNode(currentNode);

        //Then move on to the next node
        currentNode = nextNode;

    }

    //Once all nodes have been deleted, delete the list itself
    free(listToDelete);

}
