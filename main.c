#include "nodelistfunctions.h"
#include "graphprocessing.h"
#include "commands.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main(int argc, char* argv[])
{

    //Make sure there are the right number of command line arguments
    if(argc == 1)
    {

        printf("Error: Please provide an input file\n");
        return 0;

    }

    if(argc > 2)
    {

        printf("Error: Too many command line arguments\n");
        return 0;
    }

    //Create the graph from the file, exiting if there was an issue
    NodeList* myGraph = createGraph(argv[1]);

    if(myGraph == NULL)
        return 0;

    printf("Enter commands below. Use \"help\" to see list of commands\n");

    //Start accepting commands from the user
    commandLoop(myGraph);

    deleteNodeList(myGraph);

}
