#ifndef NODELISTSTRUCTURE
#define NODELISTSTRUCTURE

#include "nodeliststructure.h"

struct link
{

    long linkId;            //The ID that uniquely identifies the link
    struct node* adjNode;   //A pointer the node at the other end of the link (Each node has a list of links, so the start node is implicit)
    double length;          //The length of the link
    struct link* nextLink;  //A pointer to the next link in the linked list

};

typedef struct link Link;


struct linklist
{

    Link* firstLink;       //A pointer to the first link in the list

};

typedef struct linklist LinkList;

#endif
