#include "commands.h"
#include "graphprocessing.h"
#include "nodelistfunctions.h"
#include "fileinout.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void commandLoop(NodeList* inputGraph)
{

    int distBool = 0;
    int linkBool = 0;
    int pathBool = 0;
    NodeList* recentPath = NULL;
    NodeList* graph = inputGraph;

    //Enter a while loop
    while(1)
    {

        char userCommand[100];

        //Get the user to enter a command and read it in
        printf("=> ");
        fgets(userCommand, 100, stdin);
        int k = 0;

        //Truncate the newline character
        for(k; userCommand[k] != '\0'; k++)
        {

            if(userCommand[k] == '\n')
                userCommand[k] = '\0';

        }

        //Parse the command
        int parseValue = parseCommand(userCommand, &distBool, &linkBool, &pathBool, &recentPath, graph);

        //Exit if necessary
        if(parseValue == -1)
            break;

    }

    if(recentPath != NULL)
        deleteNodeList(recentPath);

}


int parseCommand(char* command, int* distBoolPtr, int* linkBoolPtr, int* pathBoolPtr, NodeList** recentPathPtr, NodeList* graph)
{

    //If the command is "exit", return -1 to tell the program to exit
    if(strcmp(command, "exit") == 0)
        return -1;

    //If the command is "help", run the help function
    if(strcmp(command, "help") == 0)
    {

        help();
        return 0;

    }

    //If the command is to set one of the variables, set the respective variable
    if(strcmp(command, "distance=on") == 0)
    {
        *distBoolPtr = 1;
        return 0;
    }

    if(strcmp(command, "distance=off") == 0)
    {
        *distBoolPtr = 0;
        return 0;
    }

    if(strcmp(command, "links=on") == 0)
    {
        *linkBoolPtr = 1;
        return 0;
    }

    if(strcmp(command, "links=off") == 0)
    {
        *linkBoolPtr = 0;
        return 0;
    }

    if(strcmp(command, "path=on") == 0)
    {
        *pathBoolPtr = 1;
        return 0;
    }

    if(strcmp(command, "path=off") == 0)
    {
        *pathBoolPtr = 0;
        return 0;
    }


    //If the command is "print", call the print function
    if(strcmp(command, "print") == 0)
    {

        print(graph, *recentPathPtr, *pathBoolPtr);
        return 0;

    }

    //Get the first word of the command
    char firstWord[50] = "";
    int i = 0;

    for(i; ((command[i] != ' ') && (command[i] != '\0')); i++)
        firstWord[i] = command[i];

    firstWord[i] = '\0';

    //If the first word is "path", start getting the rest of the command"
    if(strcmp(firstWord, "path") == 0)
    {

        //If the command is just "path" and no IDs, print an error
        if(command[i] == '\0')
        {

            printf("Error: No node IDs provided\n");
            return 0;

        }

        i++;

        int j = 0;
        char firstId[50] = "";
        char secondId[50] = "";

        //Get the first id provided
        for(i; (command[i] != ' ' && command[i] != '\0'); i++)
        {

            firstId[j] = command[i];
            j++;

        }

        firstId[j] = '\0';

        long firstIdValue = strtol(firstId, NULL, 10);

        //If the command is just "path [startnode id]", print an error
        if(command[i] == '\0')
        {

            printf("Error: No destination node ID provided\n");
            return 0;

        }

        i++;

        //Otherwise, get the second id provided
        j = 0;
        for(i; command[i] != '\0'; i++)
        {

            secondId[j] = command[i];
            j++;

        }

        secondId[j] = '\0';

        long secondIdValue = strtol(secondId, NULL, 10);

        //Pass the IDs and necessary variables to the path function, saving the path to recentPath
        *recentPathPtr = aStarAlgorithm(graph, firstIdValue, secondIdValue, *distBoolPtr, *linkBoolPtr);
        return 0;

    }

    printf("Error: Invalid command. Use \"help\" to see list of commands\n");
    return 0;

}


void help()
{

    printf("\n\n");
    printf("============\n");
    printf("| Commands |\n");
    printf("============\n");
    printf("\n");
    printf("help                                Prints out list of commands\n");
    printf("exit                                Exits program\n");
    printf("path [startnode id] [destnode id]   Generates the shortest path between the two nodes provied\n");
    printf("distance=on/off                     Decides whether to print out the total distance when a path is generated\n");
    printf("links=on/off                        Decides whether to print out the number of links used when a path is generated\n");
    printf("print                               Prints the map information to a file that can be displayed in gnuplot\n");
    printf("path=on/off                         Decides whether to print the most recently generated path over the map\n");
    printf("\n\n");

}


void print(NodeList* graphToPrint, NodeList* pathToPrint, int pathBool)
{

    //If the user wants to print a path...
    if(pathBool == 1)
    {

        //And there exists a path to print...
        if(pathToPrint != NULL)
        {

            //Print out the graph and path
            printGraph(graphToPrint, pathToPrint);
            return;

        }

        //If there isn't a path to print, tell the user
        printf("Error: No path to print\n");
        return;

    }

    //Otherwise, just print the graph
    printGraph(graphToPrint, NULL);
    return;

}
