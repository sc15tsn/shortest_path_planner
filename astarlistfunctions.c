#include "astarlistfunctions.h"
#include <stdio.h>
#include <stdlib.h>

AStarNode* createAStarNode(Node* newNodeRef, double newGValue, double newFValue, AStarNode* newBestPrevNode)
{

    //Allocate memory for the AStarNode, and initialise the values
    AStarNode* newAStarNode = (AStarNode*) malloc(sizeof(AStarNode));

    newAStarNode -> nodeRef = newNodeRef;
    newAStarNode -> gValue = newGValue;
    newAStarNode -> fValue = newFValue;
    newAStarNode -> bestPrevNode = newBestPrevNode;
    newAStarNode -> nextNode = NULL;

    return newAStarNode;

}


void deleteAStarNode(AStarNode* nodeToDelete)
{

    //Set all of the pointers in the node to NULL
    nodeToDelete -> nodeRef = NULL;
    nodeToDelete -> bestPrevNode = NULL;
    nodeToDelete -> nextNode = NULL;

    //Then free up the memory being used by the node
    free(nodeToDelete);

}


AStarList* createAStarList()
{

    //Allocate memory for the AStarList, and set the first node to NULL
    AStarList* newAStarList = (AStarList*) malloc(sizeof(AStarList));

    newAStarList -> firstAStarNode = NULL;

}


void addToAStarList(AStarList* listToAddTo, AStarNode* nodeToAdd)
{

    //If the list is empty, add the node to the start of the list
    if(listToAddTo -> firstAStarNode == NULL)
    {

        listToAddTo -> firstAStarNode = nodeToAdd;
        nodeToAdd -> nextNode = NULL;
        return;

    }

    //Otherwise, move to the end of the list
    AStarNode* currentNode = listToAddTo -> firstAStarNode;
    while(currentNode -> nextNode != NULL)
        currentNode = currentNode -> nextNode;

    //And add the new node to the end
    currentNode -> nextNode = nodeToAdd;
    nodeToAdd -> nextNode = NULL;

}


AStarNode* getMinDistNode(AStarList* listToSearch)
{

    AStarNode* minNode = NULL;

    //If the list is empty, return NULL
    if(listToSearch -> firstAStarNode == NULL)
        return NULL;

    //Otherwise, while there are still more nodes to evaluate...
    AStarNode* currentNode = listToSearch -> firstAStarNode;

    while(currentNode != NULL)
    {

        //If the current node has a smaller fValue, update minNode (or minNode had no stored value)
        if(minNode == NULL || (currentNode -> fValue < minNode -> fValue))
            minNode = currentNode;

        //Move on to the next node
        currentNode = currentNode -> nextNode;

    }

    //Once the entire list has been evaluated, return the min nodes
    return minNode;


}


void moveAStarNode(AStarList* sourceList, AStarNode* nodeToMove, AStarList* destList)
{

    //Get the first node in the source list
    AStarNode* currentNode = sourceList -> firstAStarNode;

    if(currentNode == nodeToMove)
    {

        sourceList -> firstAStarNode = nodeToMove -> nextNode;
        nodeToMove -> nextNode = NULL;
        addToAStarList(destList, nodeToMove);
        return;

    }

    //Find the previous node in the source list
    while(currentNode -> nextNode != nodeToMove)
        currentNode = currentNode -> nextNode;

    //Make the previous node skip over the node to move
    currentNode -> nextNode = nodeToMove -> nextNode;

    //Make the node to move stop pointing to the next node, and add it to the destination list
    nodeToMove -> nextNode = NULL;
    addToAStarList(destList, nodeToMove);

}


AStarNode* findAStarNodeInList(AStarList* listToSearch, long idToFind)
{

    //Get the first node in the list
    AStarNode* currentNode = listToSearch -> firstAStarNode;

    //While there are still more nodes to check...
    while(currentNode != NULL)
    {

        //If the underlying Node's ID matches, return this AStarNode
        if(currentNode -> nodeRef -> nodeId == idToFind)
            return currentNode;

        //Otherwise, move on to the next nodeId
        currentNode = currentNode -> nextNode;

    }

    return NULL;

}


void deleteAStarList(AStarList* listToDelete)
{

    //Get the first node in the list
    AStarNode* currentNode = listToDelete -> firstAStarNode;
    AStarNode* nextNodeToDelete = NULL;

    //While there are still nodes to delete...
    while(currentNode != NULL)
    {

        //Save the pointer to the next nodeId
        nextNodeToDelete = currentNode -> nextNode;

        //Delete the nodes
        deleteAStarNode(currentNode);

        //And move on to the next nodes
        currentNode = nextNodeToDelete;

    }

    //Once all nodes have been deleted, delete the list
    free(listToDelete);

}
