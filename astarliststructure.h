#include "nodeliststructure.h"

struct astarnode
{

    Node* nodeRef;                    //A pointer to the underlying node structure
    double gValue;                    //The distance between this node and the start node
    double fValue;                    //gValue + the shortest possible distance between this node and the goal node
    struct astarnode* bestPrevNode;   //The node that can be used to most efficiently access this node
    struct astarnode* nextNode;       //The unordered next node used to navigate the structure as a linked list

};

typedef struct astarnode AStarNode;


struct astarlist
{

    AStarNode* firstAStarNode;         //A pointer to the first AStarNode in the AStarList

};

typedef struct astarlist AStarList;
