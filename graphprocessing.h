#include "astarlistfunctions.h"


char* getLinkInfo(char*, int*);
char* getNodeInfo(char*, int*);
NodeList* setupNodes(char*);
int connectNodes(NodeList*, char*);
NodeList* createGraph(char*);

NodeList* aStarAlgorithm(NodeList*, long, long, int, int);
double calculateHeuristic(AStarNode*, AStarNode*);
NodeList* getPath(AStarList*, AStarNode*, int, int);
