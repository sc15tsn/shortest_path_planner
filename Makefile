graphtraverse: main.o nodelistfunctions.o linklistfunctions.o fileinout.o astarlistfunctions.o graphprocessing.o commands.o
	gcc -o graphtraverse main.o nodelistfunctions.o linklistfunctions.o fileinout.o astarlistfunctions.o graphprocessing.o commands.o -lm
	mkdir output

main.o: main.c nodelistfunctions.h linklistfunctions.h
	gcc -c -o main.o main.c

nodelistfunctions.o: nodelistfunctions.c nodelistfunctions.h linklistfunctions.h
	gcc -c -o nodelistfunctions.o nodelistfunctions.c

linklistfunctions.o: linklistfunctions.c linklistfunctions.h
	gcc -c -o linklistfunctions.o linklistfunctions.c

astarlistfunctions.o: astarlistfunctions.c astarlistfunctions.h
	gcc -c -o astarlistfunctions.o astarlistfunctions.c

fileinout.o: fileinout.c fileinout.h nodelistfunctions.h
	gcc -c -o fileinout.o fileinout.c

graphprocessing.o: graphprocessing.c graphprocessing.h fileinout.h nodelistfunctions.h linklistfunctions.h
	gcc -c -o graphprocessing.o graphprocessing.c

commands.o: commands.c graphprocessing.h fileinout.h
	gcc -c -o commands.o commands.c

clean:
	rm *.o graphtraverse -rf output/
