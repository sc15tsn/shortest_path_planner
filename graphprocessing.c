#include "graphprocessing.h"
#include "nodelistfunctions.h"
#include "linklistfunctions.h"
#include "fileinout.h"
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

char* getLinkInfo(char* graphString, int* charOffsetPtr)
{

    //Get the position of the start of the first link tag
    int i = 0;

    char* tempStorage = (char*)malloc(6);

    //Move to the end of the link tags
    while(1)
    {

        //Save the first 6 characters of the line into temp storage
        int j = 0;
        for(j = 0; j < 6; j++)
            tempStorage[j] = graphString[i + j];

        //If the string is the first node tag, break
        if(strcmp(tempStorage, "<node ") == 0)
            break;

        //Otherwise move to the next line
        for(i; graphString[i] != '\n'; i++);
        i++;

    }

    //Allocate the number of bytes taken up by link tags, split the string, and read the portion containing the links into a variable
    char* linkInfo = (char*)malloc(i);
    graphString[i - 1] = '\0';
    char* tempLinkInfo = &graphString[*charOffsetPtr];
    strcpy(linkInfo, tempLinkInfo);

    //Unsplit the graph details string
    graphString[i - 1] = '\n';

    free(tempStorage);

    //Save the position of the beginning of the first node tag as the offset
    *charOffsetPtr = i;

    return linkInfo;


}


char* getNodeInfo(char* graphString, int* charOffsetPtr)
{

    //Jump to the beginning of the node tags and move to the end of the entire string
    int i = *charOffsetPtr;
    for(i; graphString[i] != '\0'; i++);

    //Allocate the number of bytes used by the node tags, create a string that starts at the beginning of the node tags, and copy the contents to a new string
    char* nodeInfo = (char*)malloc(i - (*charOffsetPtr - 1));

    char* tempNodeInfo = &graphString[*charOffsetPtr];
    strcpy(nodeInfo, tempNodeInfo);


    return nodeInfo;



}


NodeList* setupNodes(char* nodeInfo)
{

    //Create a list to hold the nodes
    NodeList* graphNodes = createNodeList();

    int nodeCount = 0;
    int i = 0;

    char* tempStorage = (char*)malloc(4);

    //Start reading lines of the nodeInfo until the end is reached
    while(nodeInfo[i] == '<')
    {

        nodeCount++;

        free(tempStorage);
        tempStorage = (char*)malloc(4);

        //Start searching for an id field
        while(1)
        {

            //Read in three characters from the nodeInfo
            int j = 0;
            for(j; j < 3; j++)
                tempStorage[j] = nodeInfo[i + j];

            tempStorage[3] = '\0';

            //If the end of the line has been reached, print an error message
            if(tempStorage[2] == '\n')
            {

                printf("Error: No id field in node tag (Node %d)\n", nodeCount);
                return NULL;

            }

            //If the id field has been found, move on to storing the value
            if(strcmp(tempStorage, "id=") == 0)
                break;

            i++;

        }

        //Move past the id=
        i += 3;

        //Get the length of the value, then copy it into a string
        int k = 1;
        int a = 0;
        for(k; nodeInfo[i + k] != ' '; k++);
        char* idString = (char*)malloc(k);
        for(a; a < k; a++)
            idString[a] = nodeInfo[i + a];

        //Store the value as a long
        long idValue = strtol(idString, NULL, 10);

        free(idString);

        //Move past the id value and space
        i += (k + 1);

        free(tempStorage);
        tempStorage = (char*)malloc(5);


        //Start searching for a lat field
        while(1)
        {

            int j = 0;
            for(j; j < 4; j++)
                tempStorage[j] = nodeInfo[i + j];

            tempStorage[4] = '\0';


            if(tempStorage[3] == '\n')
            {

                printf("Error: No lat field in node tag (Node %d)\n", nodeCount);
                return NULL;

            }

            if(strcmp(tempStorage, "lat=") == 0)
                break;

            i++;

        }

        //Skip over the lat=
        i += 4;

        //Store and convert the lat value to a double
        for(k = 1; nodeInfo[i + k] != ' '; k++);
        char* latString = (char*)malloc(k);
        for(a = 0; a < k; a++)
            latString[a] = nodeInfo[i + a];

        double latValue = strtod(latString, NULL);

        free(latString);

        //Skip over the lat value and space
        i += (k + 1);

        free(tempStorage);
        tempStorage = (char*)malloc(5);

        //Start searching for a lon field
        while(1)
        {

            int j = 0;
            for(j; j < 4; j++)
                tempStorage[j] = nodeInfo[i + j];

            tempStorage[4] = '\0';

            if(tempStorage[3] == '\n')
            {

                printf("Error: No lon field in node tag (Node %d)\n", nodeCount);
                return NULL;

            }

            if(strcmp(tempStorage, "lon=") == 0)
                break;

            i++;

        }

        //Move past the lon=
        i += 4;

        //Read and convert the lon value to a double
        for(k = 1; nodeInfo[i + k] != ' '; k++);
        char* lonString = (char*)malloc(k);
        for(a = 0; a < k; a++)
            lonString[a] = nodeInfo[i + a];

        double lonValue = strtod(lonString, NULL);

        free(lonString);

        //Move past the closing tag
        for(i; nodeInfo[i] != '>'; i++);
        i += 2;

        //Once all values have been read in, create a new node from the values, and add it to the list
        addToNodeList(graphNodes, createNode(idValue, latValue, lonValue));


    }

    free(tempStorage);
    printf("Nodes created...\n");

    //Once all the nodes have been added to the list, return the list
    return graphNodes;

}


int connectNodes(NodeList* graphNodes, char* linkInfo)
{

    int linkCount = 0;
    int i = 0;

    char* tempStorage = (char*)malloc(4);

    //Start reading in links while there are still more to read
    while(linkInfo[i] == '<')
    {

        linkCount++;

        free(tempStorage);
        tempStorage = (char*)malloc(4);

        //Start searching for the id field
        while(1)
        {

            //Read in three characters
            int j = 0;
            for(j; j < 3; j++)
                tempStorage[j] = linkInfo[i + j];
            tempStorage[3] = '\0';

            //If the end of the line has been reached, print an error
            if(tempStorage[2] == '\n')
            {

                printf("Error: No id field in link tag (Link %d)\n", linkCount);
                return -1;

            }

            //If the id field is found, move on to processing the id value
            if(strcmp(tempStorage, "id=") == 0)
                break;

            i++;

        }

        //Move past the id=
        i += 3;

        //Get the length of the value and copy it into a string
        int k = 1;
        int a = 0;
        for(k; linkInfo[i + k] != ' '; k++);
        char* idString = (char*)malloc(k);
        for(a; a < k; a++)
            idString[a] = linkInfo[i + a];
        idString[k] = '\0';

        //Convert the string to a long and save the value
        long idValue = strtol(idString, NULL, 10);

        free(idString);

        //Move past the value and the space
        i += (k + 1);



        //Start searching for the two node fields
        free(tempStorage);
        tempStorage = (char*)malloc(6);

        int nodeFieldsFound = 0;
        long nodeValueOne = 0;
        long nodeValueTwo = 0;

        for(nodeFieldsFound; nodeFieldsFound < 2; nodeFieldsFound++)
        {

            while(1)
            {

                int j = 0;
                for(j; j < 5; j++)
                    tempStorage[j] = linkInfo[i + j];
                tempStorage[5] = '\0';

                if(tempStorage[4] == '\n')
                {

                    printf("Error: No node field in link tag (Link %d)\n", linkCount);
                    return -1;

                }

                if(strcmp(tempStorage, "node=") == 0)
                    break;

                i++;

            }

            //Move past the node=
            i += 5;

            //Read and convert the node ids to longs, storing in seperate variables
            for(k = 1; linkInfo[i + k] != ' '; k++);
            char* nodeString = (char*)malloc(k);
            for(a = 0; a < k; a++)
                nodeString[a] = linkInfo[i + a];
            nodeString[k] = '\0';

            if(nodeFieldsFound == 0)
                nodeValueOne = strtol(nodeString, NULL, 10);

            else if(nodeFieldsFound == 1)
                nodeValueTwo = strtol(nodeString, NULL, 10);

            free(nodeString);

            //Move over the node value, and move on to finding the next node value
            i += (k + 1);

        }

        //Once both node fields have been found, move on to finding the length field
        free(tempStorage);
        tempStorage = (char*)malloc(8);

        while(1)
        {

            int j = 0;
            for(j; j < 7; j++)
                tempStorage[j] = linkInfo[i + j];
            tempStorage[7] = '\0';

            if(tempStorage[6] == '\n')
            {

                printf("Error: No length field in link tag (Link %d)\n", linkCount);
                return -1;

            }

            if(strcmp(tempStorage, "length=") == 0)
                break;

            i++;

        }

        //Move past the length=
        i += 7;

        //Read and convert the length to a double
        for(k = 1; linkInfo[i + k]; k++);
        char* lengthString = (char*)malloc(k);
        for(a = 0; a < k; a++)
            lengthString[a] = linkInfo[i + a];

        double lengthValue = strtod(lengthString, NULL);

        free(lengthString);

        //Move past the closing tag of the lines
        for(i; linkInfo[i] != '>'; i++);
        i += 2;

        //Find the nodes at either end of the link
        Node* nodeOne = findNodeInList(graphNodes, nodeValueOne);
        Node* nodeTwo = findNodeInList(graphNodes, nodeValueTwo);

        //If either node cannot be found, print an error
        if(nodeOne == NULL)
        {

            printf("Error: Node with id %ld could not be found\n", nodeValueOne);
            return -1;

        }

        if(nodeTwo == NULL)
        {

            printf("Error: Node with id %ld could not be found\n", nodeValueTwo);
            return -1;

        }

        //Otherwise, add the link to the two nodes' list of links
        addToLinkList(nodeOne -> nodeLinks, createLink(idValue, nodeTwo, lengthValue));
        addToLinkList(nodeTwo -> nodeLinks, createLink(idValue, nodeOne, lengthValue));


    }

    free(tempStorage);

    printf("Nodes connected...\n");
    return 0;

}


NodeList* createGraph(char* mapFileString)
{

    int graphInfoOffset = 0;

    //Try to open the file specified, returning null if the file could not be opened
    FILE* inFile = openFile(mapFileString);

    if(inFile == NULL)
        return NULL;

    //Store the relevant file contents into a string, returning null if the file was formatted incorrectly
    char* graphInfo = storeFile(inFile);

    if(graphInfo == NULL)
        return NULL;

    //Split the file contents into links and nodes
    char* linkInfo = getLinkInfo(graphInfo, &graphInfoOffset);
    char* nodeInfo = getNodeInfo(graphInfo, &graphInfoOffset);

    //Create the nodes of the graph, returning null if some fields were missing
    NodeList* graph = setupNodes(nodeInfo);

    if(graph == NULL)
        return NULL;

    //Add the links to the graph, returning null if some fields were missing
    int error = connectNodes(graph, linkInfo);

    if(error == -1)
        return NULL;

    printf("Graph created.\n");

    free(graphInfo);
    free(nodeInfo);
    free(linkInfo);

    //If everything went ok, return the constructed graph
    return graph;

}


double calculateHeuristic(AStarNode* firstNode, AStarNode* secondNode)
{


    double latOne = firstNode -> nodeRef -> latitude;
    double latTwo = secondNode -> nodeRef -> latitude;
    double lonOne = firstNode -> nodeRef -> longitude;
    double lonTwo = secondNode -> nodeRef -> longitude;

    //Calculate the horizontal and vertical distance between the two nodes
    double horizontal = (lonOne - lonTwo);
    double vertical = (latOne - latTwo);

    //Calculate the direct distance between the two nodes
    double distance = sqrt((horizontal * horizontal) + (vertical * vertical));

    return distance;

}


NodeList* aStarAlgorithm(NodeList* graph, long startID, long destID, int distBool, int linkBool)
{

    //Find the start and end nodes, printing an error if they do not exist
    Node* tempStartNode = findNodeInList(graph, startID);
    if(tempStartNode == NULL)
    {

        printf("Error: Start node cannot be found\n");
        return NULL;

    }

    Node* tempDestNode = findNodeInList(graph, destID);
    if(tempDestNode == NULL)
    {

        printf("Error: Destination node cannot be found\n");
        return NULL;

    }

    //Turn the start node into an AStarNode to be used in the algorithm
    AStarNode* startNode = createAStarNode(tempStartNode, 0, -1, NULL);
    AStarNode* destNode = createAStarNode(tempDestNode, -1, -1, NULL);

    startNode -> fValue = calculateHeuristic(startNode, destNode);

    //Create lists to hold discovered nodes and evaluated nodes
    AStarList* discNodes = createAStarList();
    AStarList* evalNodes = createAStarList();

    addToAStarList(discNodes, startNode);

    //While there are still more nodes to be evaluated
    while(discNodes -> firstAStarNode != NULL)
    {

        //Start evaluating the node with the shortest distance to the destination
        AStarNode* currentNode = getMinDistNode(discNodes);

        //Move the node to the evaluated list
        moveAStarNode(discNodes, currentNode, evalNodes);


        //If the current node was the destination node, get and return the path
        if(currentNode -> nodeRef == destNode -> nodeRef)
        {

            deleteAStarNode(destNode);
            deleteAStarList(discNodes);
            return getPath(evalNodes, currentNode, distBool, linkBool);

        }


        //Otherwise, begin searching the neighbours of the current node
        Link* currentLink = currentNode -> nodeRef -> nodeLinks -> firstLink;
        while(currentLink != NULL)
        {

            Node* currentNeighbour = currentLink -> adjNode;

            //If the neighour has already been evaluated, move on to the next
            if(findAStarNodeInList(evalNodes, currentNeighbour -> nodeId) != NULL)
            {

                currentLink = currentLink -> nextLink;
                continue;

            }

            //Otherwise, calculate the distance of this node from the start
            double tempGValue = (currentNode -> gValue) + (currentLink -> length);

            //Try to find the neighbour in discNodes
            AStarNode* neighbourInList = findAStarNodeInList(discNodes, currentNeighbour -> nodeId);

            AStarNode* aStarNeighbour;

            //If the neighbour is not in discNodes, add it
            if(neighbourInList == NULL)
            {

                aStarNeighbour = createAStarNode(currentNeighbour, -1, 0, NULL);
                addToAStarList(discNodes, aStarNeighbour);

            }

            //Otherwise set it to the found node
            else
                aStarNeighbour = neighbourInList;

            //If the new gValue is smaller, or this is the first time inspecting the node, update the values
            if((tempGValue < aStarNeighbour -> gValue) || (aStarNeighbour -> gValue == -1))
            {

                aStarNeighbour -> bestPrevNode = currentNode;
                aStarNeighbour -> gValue = tempGValue;
                aStarNeighbour -> fValue = (aStarNeighbour -> gValue) + calculateHeuristic(aStarNeighbour, destNode);

            }

            //Move on to the next link
            currentLink = currentLink -> nextLink;

        }

    }

    //If there are no more nodes to search and a path has not been found, print an error
    printf("Error: Path could not be found\n");
    deleteAStarList(discNodes);
    deleteAStarList(evalNodes);
    return NULL;

}


NodeList* getPath(AStarList* evalNodes, AStarNode* destNode, int distBool, int linkBool)
{


    double distance = destNode -> gValue;
    int nodeCount = 0;

    //Create a nodelist to hold the path
    NodeList* path = createNodeList();

    //Start at the destination node
    AStarNode* currentNode = destNode;

    //While the path has not ended
    while(currentNode != NULL)
    {

        //Create a new node from the underlying node's details
        long newId = currentNode -> nodeRef -> nodeId;
        double newLat = currentNode -> nodeRef -> latitude;
        double newLon = currentNode -> nodeRef -> longitude;

        Node* nodeInPath = createNode(newId, newLat, newLon);

        //Add the node to the path
        addToNodeList(path, nodeInPath);

        //Move on to the next node
        currentNode = currentNode -> bestPrevNode;

        //Increment the links used
        nodeCount++;

    }

    //After adding the nodes to the path, go through again adding links between each of the nodes
    Node* currentPathNode = path -> firstNode;

    while(currentPathNode -> nextNode != NULL)
    {

        addToLinkList(currentPathNode -> nodeLinks, createLink(0, currentPathNode -> nextNode, 0));
        currentPathNode = currentPathNode -> nextNode;

    }

    //The links used will be nodeCount - 1
    int linksUsed = nodeCount - 1;

    //Print out any optional values
    if(distBool == 1)
        printf("Total Distance: %lf\n", distance);
    if(linkBool == 1)
        printf("Links Used: %d\n", linksUsed);

    printf("Shortest path found.\n");

    //Delete the AStarList and return the path
    deleteAStarList(evalNodes);
    return path;

}
