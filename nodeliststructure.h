#ifndef LINKLISTSTRUCTURE
#define LINKLISTSTRUCTURE

#include "linkliststructure.h"


struct node
{

    long nodeId;                 //The ID that uniquely identifies the node
    double latitude;             //The latitude of the node
    double longitude;            //The longitude of the node
    struct linklist* nodeLinks;  //A pointer to a linked list of links attached to this node
    struct node* nextNode;       //A pointer to the next node in the linked list

};

typedef struct node Node;



struct nodelist
{

    Node* firstNode;      //The first node in the linked list

};

typedef struct nodelist NodeList;


#endif
