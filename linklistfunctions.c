#include "linklistfunctions.h"
#include <stdlib.h>

Link* createLink(long newId, Node* newAdjNode, double newLength)
{

    //Allocate enough memory for the Link
    Link* newLink = (Link*) malloc(sizeof(Link));

    //Initialise the fields with the given values
    newLink -> linkId = newId;
    newLink -> adjNode = newAdjNode;
    newLink -> length = newLength;
    newLink -> nextLink = NULL;

    return newLink;

}


void deleteLink(Link* linkToDelete)
{

    //Stop pointing to the end node
    linkToDelete -> adjNode = NULL;

    //And delete the link
    free(linkToDelete);

}


LinkList* createLinkList()
{

    //Allocate enough memory for the listToAddTo
    LinkList* newLinkList = (LinkList*) malloc(sizeof(LinkList));

    //Initialise the fields and return the listToAddTo
    newLinkList -> firstLink = NULL;

    return newLinkList;

}


void addToLinkList(LinkList* listToAddTo, Link* linkToAdd)
{

    //Make the new link the start of the list, and make the new link point to the previous start of the list
    linkToAdd -> nextLink = listToAddTo -> firstLink;
    listToAddTo -> firstLink = linkToAdd;

}


void deleteLinkList(LinkList* listToDelete)
{

    //Get the first link
    Link* currentLink = listToDelete -> firstLink;

    //While the end of the list hasn't been reached
    while(currentLink != NULL)
    {

        //Get the next link in the list
        Link* nextLink = currentLink -> nextLink;

        //Delete the current link
        deleteLink(currentLink);

        //Then move on to the next link
        currentLink = nextLink;

    }

    //Once all of the links have been deleted, delete the list itself
    free(listToDelete);

}
