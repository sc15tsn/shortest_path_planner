#include "nodeliststructure.h"
#include <stdio.h>

/********************************************************************
* openFile - A function that attempts to open a file and prints an  *
* error message otherwise                                           *
*                                                                   *
* Inputs: The file name of the file to open                         *
*                                                                   *
* Outputs: Returns a pointer to the opened file, or returns NULL if *
* there was an error. Prints an error message if there was an error *
********************************************************************/
FILE* openFile(char*);

/********************************************************************
* validateFile - A function that makes sure the file containing     *
* graph information contains all the necessary components           *
*                                                                   *
* Inputs: A pointer to the file to validate                         *
*                                                                   *
* Outputs: Returns 1 if there was an error, otherwise returns 0.    *
* Prints out an appropriate error message if there is an error      *
********************************************************************/
int validateFile(FILE*);

/********************************************************************
* storeFile - A function that copies the file contents into a       *
* string variable, checking to make sure to check the file is in    *
* the correct format                                                *
*                                                                   *
* Inputs: A pointer to the file to attempt to store                 *
*                                                                   *
* Ouptputs: A string containing the file contents                   *
********************************************************************/
char* storeFile(FILE*);

/********************************************************************
* printGraph - A function that sets up necessary output files and   *
* prints out the details needed to display a graph and path in      *
* gnuplot                                                           *
*                                                                   *
* Inputs: A pointer to the graph to print, a pointer to the         *
* path (or NULL if none) in the graph
*                                                                   *
* Outputs: No values returned. Sets up graph.out containing graph   *
* plotting details, path.out (if path provided) containing path     *
* plotting details, and print.gnu which can be used to print the    *
* graph and path in gnuplot                                         *
********************************************************************/
void printGraph(NodeList*, NodeList*);
